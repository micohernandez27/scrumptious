from django.contrib import admin
from .models import Recipe, RecipeStep, RecipeIngredient
# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ("title", "id")

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "recipe_title",
        "id",
        "order",
    )

@admin.register(RecipeIngredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display = (
        "recipe_title",
        "ingredient",
        "amount",
    )
